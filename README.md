Jose Andres Murillo Sancho
1. Límite para la entrega de la asignación: Lunes 2 de septiembre a las 8am.
2. Plataforma de revisión: Repositorio de código git y blog del curso.
3. Cada archivo debe estar debidamente documentado con la información personal del
estudiante que lo escriba, además de explicar su código e indicar cualquier
referencia a código de terceros
4. Se debe incluir un archivo README que contenga el enunciado de los ejercicios.