/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante: Jose Andres Murillo Sancho
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- Retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parámetros
	Salidas: No retorma parámetros
	Funcionamiento: 
    - Asigna 0 a ref_lista->cantidad.
    - Asigna NULL a ref_lista->ref_inicio.
    - Se reserva espacio en memoria.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: nodo_estudiante* nuevo
	Salidas: No retorma parámetros
	Funcionamiento: 
    - Funciona para incsertar un nuevo elemento al inicio de la lista
    - Si ref_lista == NULL se llama a inicializar_lista.
    - Si no se cumple el if ref_lista->cantidad aumenta.
    - Si no se cumple el if a nuevo->ref_siguiente se le asigna ref_lista->ref_inicio.
    - Si no se cumple el if a ref_lista->ref_inicio se le asigna nuevo.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: nodo_estudiante* nuevo
	Salidas:  No retorma parámetros
	Funcionamiento: 
    - Funciona para incsertar un nuevo elemento al final de la lista
    - Si ref_lista == NULL se llama a inicializar_lista.
    - Si ref_lista->cantidad == 0 se llama a insertar_inicio(nuevo).
    - Se crea un nodo_estudiante llamado temporal.
    - Se reserva espacio en memoria.
    - Se le asigna a temporal el valor de ref_lista->ref_inicio.
    - Se recorre todo temporal hasta la posicion final y se le asigna a temporal->ref_siguiente la variable nuevo.
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: int indice
	Salidas: No retorma parámetros
	Funcionamiento: 
   - Funciona para borrar un elemento.
   - Se crea un nodo_estudiante llamado temporal.
   - Se reserva espacio en memoria.
   - Se le asigna a temporal el valor de NULL.
   - Se crea un int llamado cont y se le asigna el valor de 0.
   - Si ref_lista->ref_inicio == NULL se retorna nada.
   - Si indice >= ref_lista->cantidad se retorna nada.
   - Si indice >= ref_lista->cantidad se retorna nada.
   - Se le asigna a temporal el valor de ref_lista->ref_inicio
   - Si ref_lista->ref_inicio:
     - Se le asigna a ref_lista->ref_inicio el valor de  temporal->ref_siguiente
     - A ref_lista->cantidad se le resta 1
     - Se libera el espacio en memoria de temporal    
   - Si no:
     - Reorrer todo temporal y suma de 1 en 1 cont hasta que indice y cont sea iguales entonces:
       - Se le asigna a temporal->ref_siguiente el valor de temporal->ref_siguiente.
       -  A ref_lista->cantidad se le resta 1.
---------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: int indice
	Salidas: Retorna temporal
	Funcionamiento: 
   - Funciona para borrar un elemento.
   - Se crea un nodo_estudiante llamado temporal.
   - Se reserva espacio en memoria.
   - Se le asigna a temporal el valor de NULL.
   - Si ref_lista->ref_inicio == NULL || ref_lista->cantidad ==0 retorna temporal
   - Si no:
    - Se asigna cont el valor 0
    - Se le asigna a temporal el valor ref_lista->ref_inicio
    - Reorrer todo temporal y suma de 1 en 1 cont hasta que indice y cont  sea iguales entonces retorna temporal
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: int carnet_almacenado y int carnet_ingresado
	Salidas: No retorma parámetros
	Funcionamiento: 
   - Si carnet_almacenado == carnet_ingresado manda a pantalla  El carnet ingresado es correcto
   -Si no manda a pantalla El carnet ingresado es incorrecto
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: int carnet_almacenado y int carnet_ingresado
	Salidas: No retorma parámetros
	Funcionamiento:
   - Entra en un ciclo hasta que opcion = 0 
   - Llama imprimir() 
   - Solicita al usuario opcion
   - Si la opcion es 0 sale del programa
   - Si la opcion es 1 a temporal se le asigna crear_nodo() y se llama    insertar_final(temporal)
   - Si la opcion es 2 a temporal se le asigna crear_nodo() y se llama insertar_inicio(temporal)
   - Si la opcion es 3 se solicita al usuario el indice y se el asigna a temporal el valor buscar_por_indice(indice) y despues:
    - Se solicita al usuario el carnet y se llama validar_carnets(temporal->carnet, carnet)
   -  Si la opcion es 4 se solicita al usuario el indice y  se llama borrar_por_indice(indice)
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas:  No recibe parámetros
	Salidas: Returna 0
	Funcionamiento: 
   - Se llama menu()
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: size_t max_size
	Salidas: Returna buffer
	Funcionamiento: 
   - Se reserva espacio en memoria.
   - Si buffer == NULL se llama exit(1)
   - Se le asigna a characters el valor de getline(&buffer,&max_size,stdin)
   - Se le asigna buffer[strlen(buffer)-1] el valor de 0
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: size_t max_size
	Salidas: Retorna numerical_input
	Funcionamiento: 
   - Se le asigna a numerical_input el valor de atoi(get_user_input(max_size))
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);